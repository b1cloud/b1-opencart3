Įskiepis skirtas sinchronizuoti produktus tarp OpenCart ir B1.lt aplikacijos.

### Reikalavimai ###

* PHP 7.0
* OpenCart 3.0.3.1
* MySQL 5.7.23

### Diegimas ###

* `NEBŪTINA` Pasidarykite atsarginę failų kopiją.
* Padarykite atsarginę DB kopiją.
* Galima įrašyti :
* 1. `extension installer` ikelkite `B1Accounting.ocmod.zip`
* 2. Perkelkite `upload` direktoriją į OpenCart direktoriją.
* Administracijos skiltyje `Extensions->Modules` įdiekite "B1 accounting" modulį ir suveskite reikiamą informaciją.
* Administracijos skiltyje, "B1 accounting" modulyje 'Orders sync from' laukelyje nurodykite data, nuo kurios bus sinchronizuojami užsakymai. Datos formatas Y-m-d. Pvz. 2016-12-10. 
* Administracijos skiltyje, "B1 accounting" modulyje 'Confirmed order status ID' laukelyje nurodykite užsakymų, kurie bus sinchronizuojami su B1, statuso ID. Pagal nutylėjimą 5 (Complete).
* Prie serverio Cron darbų sąrašo pridėkite visus išvardintus cron darbus, nurodytus modulio konfigūravimo puslapyje.
  Pridėti cron darbus galite per serverio valdymo panelę (DirectAdmin, Cpanel) arba įvykdę šias komandines eilutes serverio pusėje
    * `0 */12 * * * wget -q -O - '[products_cron_url]'` Vietoj [products_cron_url] reikia nurodyti savo Cron adresą. 
    * `*/10 * * * * wget -q -O - '[orders_cron_url]'` Vietoj [orders_cron_url] reikia nurodyti savo Cron adresą. 
<HR>
Jei išjungiate ir vėl įjungiate modulį, išsisaugokite parduotuvės ID (E-shop ID), o po to vėl jį įrašykite, kad užsakymai, paspaudus `Reset B1 orders` nesidubliuotų B1 sistemoje.


### Kontaktai ###

* Kilus klausimams, prašome kreiptis info@b1.lt
