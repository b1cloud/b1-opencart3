<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// Configuration
if (is_file('../../../config.php')) {
    require_once('../../../config.php');
}

if (!defined('DIR_APPLICATION')) {
    header('Location: ../../../install/index.php');
    exit;
}
require_once(DIR_SYSTEM . 'library/vendor/b1/libb1/B1.php');
require_once(DIR_SYSTEM . 'library/request.php');
require_once(DIR_SYSTEM . 'library/db.php');
require_once(DIR_SYSTEM . 'library/db/' . DB_DRIVER . '.php');
require_once(DIR_SYSTEM . 'library/config.php');
require_once(DIR_SYSTEM . 'engine/event.php');
require_once(DIR_SYSTEM . 'engine/loader.php');
require_once(DIR_SYSTEM . 'engine/model.php');
require_once(DIR_SYSTEM . 'engine/registry.php');
if (is_file(DIR_SYSTEM . 'engine/proxy.php')) {
    require_once(DIR_SYSTEM . 'engine/proxy.php');
}

class B1Cron
{

    const TTL = 3600;
    const MAX_ITERATIONS = 100;
    const ORDERS_PER_ITERATION = 100;
    const ORDER_SYNC_THRESHOLD = 10;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var DB
     */
    private $db;

    /**
     * @var ModelB1Settings
     */
    private $settings;

    /**
     * @var ModelB1Helper
     */
    private $helper;

    /**
     * @var ModelB1Items
     */
    private $items;

    /**
     * @var ModelB1Items
     */
    private $manager;
    /**
     * @var ModelB1Orders
     */

    private $orders;


    private function init()
    {
        set_time_limit(self::TTL);
        ini_set('max_execution_time', self::TTL);

        // Registry
        $registry = new Registry();
        // Config
        $config = new Config();
        $registry->set('config', $config);
        // Event
        $event = new Event($registry);
        $registry->set('event', $event);
        // Loader
        $loader = new Loader($registry);
        $registry->set('load', $loader);
        // Request
        $registry->set('request', new Request());
        // Database
        $registry->set('db', new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE));


        $this->request = $registry->get('request');
        $this->db = $registry->get('db');

        $loader->model('extension/module/settings');
        $this->settings = $registry->get('model_extension_module_settings');
        $loader->model('extension/module/helper');
        $this->helper = $registry->get('model_extension_module_helper');
        $loader->model('extension/module/items');
        $this->items = $registry->get('model_extension_module_items');
        $loader->model('extension/module/orders');
        $this->orders = $registry->get('model_extension_module_orders');
        $loader->model('extension/module/manager');
        $this->manager = $registry->get('model_extension_module_manager');

    }

    public function __construct()
    {
        $this->init();

        if (isset($this->request->get['id'])) {
            $get_id = $this->request->get['id'];
        } else {
            $get_id = null;
        }
        try {
            switch ($get_id) {
                case 'products':
                    $this->manager->fetchProducts(true);
                    break;
                case 'orders':
                    $this->manager->syncOrders();
                    break;
                case 'importProducts':
                    $this->manager->getImportDropdownItems();
                    break;
                default:
                    throw new B1Exception('Bad action ID specified.');
            }
        } catch (B1Exception $e) {
            echo '<pre>';
            print_r($e->getMessage());
            echo '</pre>';
            echo '<pre>';
            print_r($e->getExtraData());
            echo '</pre>';
            echo '<pre>';
            print_r($e->getTrace());
            echo '</pre>';
        }

    }
}

new B1Cron;