<?php

require_once(DIR_APPLICATION . 'model/extension/module/base.php');

class ModelExtensionModuleItems extends ModelExtensionModuleBase
{

    public function resetAllB1ReferenceId()
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET b1_reference_id = NULL ");

    }

    public function updateProductReferenceId($id, $code)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET b1_reference_id = " . $this->db->escape($id) . " WHERE sku ='" . $this->db->escape($code) . "'");

    }

    public function updateProductQuantity($quantity, $code)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . $this->db->escape($quantity) . "' WHERE b1_reference_id = " . $this->db->escape($code));

    }

    public function updateItemName($name, $code)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product_description as pd
        left join " . DB_PREFIX . "product as p on pd.product_id = p.product_id
        SET pd.name = '" . $this->db->escape($name) . "' WHERE p.b1_reference_id = " . $this->db->escape($code) . "");

    }

    public function updateItemPrice($price, $code)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET price = '" . $this->db->escape($price) . "' WHERE b1_reference_id = " . $code);
    }

    public function checkShopVatSettings()
    {
        return $this->db->query("SELECT * FROM `" . DB_PREFIX . "setting` where `key` = 'config_tax'")->row;
    }

    public function fetchAllItems($limit)
    {
        $query = $this->db->query("
SELECT * FROM " . DB_PREFIX . "product p
LEFT JOIN " . DB_PREFIX . "product_description pd ON p.product_id = pd.product_id
WHERE b1_reference_id IS NULL  LIMIT " . $this->db->escape($limit));
        return $query->rows;
    }

    public function getDataBySql($sql, $id)
    {
        if ($sql) {
            foreach ($this->db->query(str_replace('%d', $id, $sql))->row as $row) {
                return $row;
            }
        }
        return false;
    }

}
