<?php

require_once(DIR_APPLICATION . 'model/extension/module/base.php');

class ModelExtensionModuleOrders extends ModelExtensionModuleBase
{
    public function assignB1OrderId($b1_order, $order_id)
    {
        if (!$b1_order){
            return $this->db->query("UPDATE " . DB_PREFIX . "order SET b1_reference_id = NULL WHERE order_id = " . $this->db->escape($order_id));
        } else {
            return $this->db->query("UPDATE " . DB_PREFIX . "order SET b1_reference_id = " . $this->db->escape($b1_order) . " WHERE order_id = " . $this->db->escape($order_id));
        }


    }

    public function getOrdersForSync($iterations, $order_status_id, $orders_sync_from, $orders_sync_to = null)
    {
        if(!empty($orders_sync_to)) {
            $query = "SELECT DISTINCT o.order_id as int_id, o.order_id, o.currency_code, o.currency_value, o.total, o.email, o.payment_company, o.payment_firstname, o.payment_lastname, o.payment_address_1, o.payment_city, o.shipping_firstname, o.shipping_lastname, o.shipping_address_1, o.shipping_city, o.shipping_company, c.*, oh.order_history_id,oh.date_added FROM " . DB_PREFIX . "order o
                    LEFT JOIN " . DB_PREFIX . "country c ON o.payment_country_id = c.country_id
                    LEFT JOIN " . DB_PREFIX . "order_history AS oh on o.order_id = oh.order_id AND oh.date_added = (
                        SELECT MAX(oh2.date_added) FROM " . DB_PREFIX . "order_history oh2
                            WHERE o.order_id = oh2.order_id AND oh2.order_status_id = '" . $this->db->escape($order_status_id) . "' AND oh2.date_added >= '" . $this->db->escape($orders_sync_from) . "' AND oh2.date_added <= '" . $this->db->escape($orders_sync_to) . "'
                    )
                    WHERE  b1_reference_id IS NULL AND o.order_status_id = '" . $this->db->escape($order_status_id) . "' AND oh.order_status_id = '" . $this->db->escape($order_status_id) . "' AND oh.date_added >= '" . $this->db->escape($orders_sync_from) . "'  AND oh.date_added <= '" . $this->db->escape($orders_sync_to) . "' ORDER BY oh.date_added ASC LIMIT " . $this->db->escape($iterations);

        } else {
            $query = "SELECT DISTINCT o.order_id as int_id, o.order_id, o.currency_code, o.currency_value, o.total, o.email, o.payment_company, o.payment_firstname, o.payment_lastname, o.payment_address_1, o.payment_city, o.shipping_firstname, o.shipping_lastname, o.shipping_address_1, o.shipping_city, o.shipping_company, c.*, oh.order_history_id,oh.date_added FROM " . DB_PREFIX . "order o
                    LEFT JOIN " . DB_PREFIX . "country c ON o.payment_country_id = c.country_id
                    LEFT JOIN " . DB_PREFIX . "order_history AS oh on o.order_id = oh.order_id AND oh.date_added = (
                        SELECT MAX(oh2.date_added) FROM " . DB_PREFIX . "order_history oh2
                            WHERE o.order_id = oh2.order_id AND oh2.order_status_id = '" . $this->db->escape($order_status_id) . "' AND oh2.date_added >= '" . $this->db->escape($orders_sync_from) . "'
                    )
                    WHERE  b1_reference_id IS NULL AND o.order_status_id = '" . $this->db->escape($order_status_id) . "' AND oh.order_status_id = '" . $this->db->escape($order_status_id) . "' AND oh.date_added >= '" . $this->db->escape($orders_sync_from) . "' ORDER BY oh.date_added ASC LIMIT " . $this->db->escape($iterations);
        }
        return $this->db->query($query);
    }


    public function getOrderStatuses($languageCode)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_status` WHERE language_id = '" . $this->db->escape($languageCode) . "' ORDER BY order_status_id");
        return $query->rows;
    }

    public function getDataBySql($sql, $id)
    {
        if ($sql) {
            foreach ($this->db->query(str_replace('%d', $id, $sql))->row as $row) {
                return $row;
            }
        }
    }

}
