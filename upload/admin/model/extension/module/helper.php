<?php

require_once(DIR_APPLICATION . 'model/extension/module/base.php');

class ModelExtensionModuleHelper extends ModelExtensionModuleBase
{

    public function printPre($value)
    {
        echo '<pre>';
        print_r($value);
        echo '</pre>';
    }

}
