<?php

class ModelExtensionModuleValidations
{
    const PER_PAGE = 20;

    public function __construct()
    {
        // Registry
        $registry = new Registry();
        // Database
        $registry->set('db', new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE));

        $this->request = $registry->get('request');
        $this->db = $registry->get('db');

    }

    public static function logsTableName()
    {
        return DB_PREFIX . 'b1_validation_logs';
    }


    public function createTable()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . self::logsTableName() . "` (
                id bigint(11) NOT NULL AUTO_INCREMENT,
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                debug_info text,
                PRIMARY KEY  (id) 
            ) ENGINE=MYISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");
    }

    public function deleteTable()
    {
        $tableName = self::logsTableName();
        $this->db->query("DROP TABLE IF EXISTS `{$tableName}`");
    }

    /**
     * Used by cron to clear log items older than 7 days
     */
    public function clearOldLogs()
    {
        try {
            $tableName = self::logsTableName();
            $this->db->query("TRUNCATE TABLE {$tableName}");

        } catch (Exception $e) {
        }
    }
    public function hasRecords()
    {
        try {
            $tableName = self::logsTableName();
            $query = $this->db->query("SELECT COUNT(*) as total FROM {$tableName}");
            $rowcount = $query->row['total'];

            return $rowcount > 0;

        } catch (B1Exception $e) {
            return 0;
        }
    }

    /**
     * Save debug info to log
     * @param $debug_info
     */
    public function save($debug_info)
    {
        try {
            $tableName = self::logsTableName();
            $value = $this->db->escape($debug_info);
            $this->db->query("INSERT INTO `{$tableName}`(`debug_info`) VALUES('{$value}')");
        } catch (Exception $e) {
        }
    }

    public function fetchAllLogsAsHtml($page = 1)
    {
        $html = '<table class="table table-hover B1-logs">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Date</th>
                            <th scope="col">Details</th>
                        </tr>
                        </thead>
                        <tbody >';


        try {
            $per_page = self::PER_PAGE;
            $offset = ($page - 1) * $per_page;

            $tableName = self::logsTableName();

            $sql = "SELECT * FROM `{$tableName}` ORDER BY `created_at` ASC LIMIT {$per_page} OFFSET {$offset}";

            $total_query = $this->db->query("SELECT COUNT(*) as total FROM {$tableName}")->row;
            $total = $total_query['total'];
            $num_of_pages = ceil($total / $per_page);

            $logs = $this->db->query($sql);

            foreach ($logs->rows as $row) {
                $short_text = $row['debug_info'];

                $html .= "<tr data-id='{$row->id}'>
                                <td>{$row['id']}</td>
                                <td>{$row['created_at']}</td>
                                <td>{$short_text}</td>
                            </tr>";
            }

            // pagination
            $range = 2; /* how many pages around page selected */
            $start = (int)($page - $range);
            if ($start < 1) {
                $start = 1;
            }
            $stop = (int)($page + $range);
            if ($stop > $num_of_pages) {
                $stop = (int)($num_of_pages);
            }

            if ($total > 0) {
                $page_links = $this->getPaginationHtml($page, $start, $stop, $num_of_pages);
            } else {
                $page_links = '';
            }

        } catch (\Exception $e) {
        }
        $html .= '</tbody></table>
                <div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
        return $html;
    }


    /**
     * Custom pagination generator
     * @param $p
     * @param $n
     * @param $start
     * @param $stop
     * @param $pages_nb
     * @return string
     */
    private function getPaginationHtml($p, $start, $stop, $pages_nb)
    {
        $html = '<div id="pagination" class="pagination">';

        if ($start!=$stop){
            $html .= '<ul class="pagination">';
            if( $p !== 1) {
                $p_previous = $p-1;
                $html .= '<li id="pagination_previous"><a rel="'.$p_previous.'" class="pagination_page_number" href="#">&laquo;&nbsp;Previous</a></li>';
            } else {
                $html .= '<li id="pagination_previous" class="disabled"><span>&laquo;&nbsp;Previous</span></li>';
            }
            if($start >3){
                $html .= '<li><a rel="1" class="pagination_page_number" href="#">1</a></li>
                    <li class="truncate">...</li>';
            }
            for( $i = $start; $i<$stop+1; $i++){
                if($p == $i) {
                    $html .= '<li class="current"><span>'.$p.'</span></li>';
                } else {
                    $html .= '<li><a rel="'.$i.'" class="pagination_page_number" href="#">'.$i.'</a></li>';
                }
            }
            if($pages_nb > $stop+2) {
                $html .= '<li class="truncate">...</li>
                    <li><a rel="'.$pages_nb.'" class="pagination_page_number" href="#">'.$pages_nb.'</a></li>';
            }
            if( $pages_nb >1 && $p !== $pages_nb) {
                $p_next = $p+1;
                $html .= '<li id="pagination_next"><a class="pagination_page_number" rel="'.$p_next.'" href="#">Next&nbsp;&raquo;</a></li>';
            } else {
                $html .= '<li id="pagination_next" class="disabled"><span>Next&nbsp;&raquo;</span></li>';
            }
            $html .= '</ul>';
        }
        $html .='</div>';
        return $html;
    }



}
