<?php

require_once(DIR_APPLICATION . 'model/extension/module/base.php');

class ModelExtensionModuleSettings extends ModelExtensionModuleBase
{

    public static function tableName()
    {
        return DB_PREFIX . 'b1_settings';
    }

    public function createTable()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . self::tableName() . "` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `key` varchar(128) NOT NULL,
            `value` varchar(500) NOT NULL,
            PRIMARY KEY (`id`)
            ) ENGINE=MYISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");
    }

    public function deleteTable()
    {
        $this->db->query("DROP TABLE IF EXISTS `" . self::tableName() . "`");
    }

    public function insertDefaultSettings()
    {
        $mappingDefaults = $this->getMappingsDefault();
        $settingDefaults = $this->getSettingsDefault();
        //configuration
        $this->add('cron_key', $this->generateCronKey());
        $this->add('api_key', '');
        $this->add('private_key', '');
        $this->add('shop_id', base_convert(time(), 10, 36));
        $this->add('b1_id', '');
        $this->add('documentation_url', 'https://www.b1.lt/doc/api');
        $this->add('help_page_url', 'https://www.b1.lt/help');
        $this->add('b1_contact_email', 'info@b1.lt');
        $this->add('orders_sync_from', $settingDefaults['orders_sync_from']);
        $this->add('orders_sync_to', $settingDefaults['orders_sync_to']);
        $this->add('order_status_id', $settingDefaults['order_status_id']);
        $this->add('quantity_sync', $settingDefaults['quantity_sync']);
        $this->add('write_off', $settingDefaults['write_off']);
        $this->add('sync_item_name', $settingDefaults['sync_item_name']);
        $this->add('sync_item_price', $settingDefaults['sync_item_price']);
        $this->add('vat_rate', 0);
        $this->add('sync_invoice', $settingDefaults['sync_invoice']);
        $this->add('cron_job', 0);
        // mapping
        $this->add('order_date', $mappingDefaults['order_date']);
        $this->add('order_no', $mappingDefaults['order_no']);
        $this->add('invoice_number', $mappingDefaults['invoice_number']);
        $this->add('invoice_series', $mappingDefaults['invoice_series']);
        $this->add('currency', $mappingDefaults['currency']);
        $this->add('shipping_amount', $mappingDefaults['shipping_amount']);
        $this->add('discount', $mappingDefaults['discount']);
        $this->add('gift', $mappingDefaults['']);
        $this->add('total', $mappingDefaults['']);
        $this->add('order_email', $mappingDefaults['']);
        $this->add('billing_is_company', $mappingDefaults['billing_is_company']);
        $this->add('billing_first_name', $mappingDefaults['billing_first_name']);
        $this->add('billing_last_name', $mappingDefaults['billing_last_name']);
        $this->add('billing_address', $mappingDefaults['billing_address']);
        $this->add('billing_city', $mappingDefaults['billing_city']);
        $this->add('billing_country', $mappingDefaults['billing_country']);
        $this->add('billing_short_name', $mappingDefaults['billing_short_name']);
        $this->add('billing_postcode', $mappingDefaults['billing_postcode']);
        $this->add('billing_vat_code', $mappingDefaults['billing_vat_code']);
        $this->add('billing_code', $mappingDefaults['billing_code']);
        $this->add('delivery_is_company', $mappingDefaults['delivery_is_company']);
        $this->add('delivery_first_name', $mappingDefaults['delivery_first_name']);
        $this->add('delivery_last_name', $mappingDefaults['delivery_last_name']);
        $this->add('delivery_address', $mappingDefaults['delivery_address']);
        $this->add('delivery_city', $mappingDefaults['delivery_city']);
        $this->add('delivery_country', $mappingDefaults['delivery_country']);
        $this->add('delivery_short_name',  $mappingDefaults['delivery_short_name']);
        $this->add('delivery_vat_code',  $mappingDefaults['delivery_vat_code']);
        $this->add('delivery_postcode',  $mappingDefaults['delivery_postcode']);
        $this->add('delivery_code',  $mappingDefaults['delivery_code']);
        $this->add('payer_name',  $mappingDefaults['payer_name']);
        $this->add('payer_code',  $mappingDefaults['payer_code']);
        $this->add('payer_vat_code',  $mappingDefaults['payer_vat_code']);
        $this->add('payer_address',  $mappingDefaults['payer_address']);
        $this->add('payer_country_code',  $mappingDefaults['payer_country_code']);
        $this->add('payment_code',  $mappingDefaults['payment_code']);
        $this->add('payment_id',  $mappingDefaults['payment_id']);
        $this->add('payment_payment_date',  $mappingDefaults['payment_payment_date']);
        $this->add('payment_sum',  $mappingDefaults['payment_sum']);
        $this->add('payment_tax',  $mappingDefaults['payment_tax']);
        $this->add('payment_currency',  $mappingDefaults['payment_currency']);
        $this->add('payment_payment',  $mappingDefaults['payment_payment']);
        $this->add('items_name', $mappingDefaults['items_name']);
        $this->add('items_quantity', $mappingDefaults['items_quantity']);
        $this->add('items_price', $mappingDefaults['items_price']);
        $this->add('items_sum', $mappingDefaults['items_sum']);
        $this->add('items_vat_rate',  $mappingDefaults['items_vat_rate']);
        $this->add('items_code',  $mappingDefaults['items_code']);
    }

    private function generateCronKey()
    {
        return hash_hmac('sha256', uniqid(rand(), true), microtime() . rand());
    }

    public function updateExternalTables()
    {
        $this->db->query("ALTER TABLE `" . self::productTableName() . "` ADD `b1_reference_id` INT(10) UNSIGNED NULL DEFAULT NULL");
        $this->db->query("ALTER TABLE `" . self::orderTableName() . "` ADD `b1_reference_id` INT(10) UNSIGNED NULL DEFAULT NULL");
        $this->load->model('user/user_group');
        $this->model_user_user_group->addPermission($this->user->getId(), 'access', 'module/b1');
        $this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'module/b1');
    }

    public function deleteExternalTableModifications()
    {
        $this->db->query("ALTER TABLE `" . self::productTableName() . "` DROP `b1_reference_id`");
        $this->db->query("ALTER TABLE `" . self::orderTableName() . "` DROP `b1_reference_id`");
        $this->load->model('user/user');
        $this->load->model('user/user_group');
        $user = $this->model_user_user->getUser($this->user->getId());
        $userGroupData = $this->model_user_user_group->getUserGroup($user['user_group_id']);
        unset($userGroupData['access']['module/b1']);
        unset($userGroupData['modify']['module/b1']);
        $this->model_user_user_group->editUserGroup($user['user_group_id'], $userGroupData);
    }

    public function get($key)
    {
        $query = $this->db->query("SELECT * FROM `" . self::tableName() . "` WHERE `key` = '" . $this->db->escape($key) . "'");
        if (isset($query->row['value'])) {
            return $query->row['value'];
        } else {
            return null;
        }
    }

    public function set($key, $data)
    {
        $this->db->query("UPDATE `" . self::tableName() . "` SET `value` = '" . $this->db->escape($data) . "' WHERE `key` = '" . $this->db->escape($key) . "'");
    }

    public function update($keys, $data)
    {
        foreach ($keys as $key) {
            $this->db->query("UPDATE `" . self::tableName() . "` SET `value` = '" . $this->db->escape($data[$key]) . "' WHERE `key` = '" . $this->db->escape($key) . "'");
        }
    }

    public function delete($key)
    {
        $this->db->query("DELETE FROM `" . self::tableName() . "` WHERE `key` = '" . $this->db->escape($key) . "'");
    }

    public function add($key, $value)
    {
        $this->db->query("INSERT INTO `" . self::tableName() . "` SET `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
    }

    public function disable($key)
    {
        $this->db->query("UPDATE `" . self::tableName() . "` SET `value` = '0' WHERE `key` = '" . $this->db->escape($key) . "'");
    }

    public function enable($key)
    {
        $this->db->query("UPDATE `" . self::tableName() . "` SET `value` = '1' WHERE `key` = '" . $this->db->escape($key) . "'");
    }

    public function resetOrdersId()
    {
        $this->db->query("UPDATE `" . self::orderTableName() . "` SET `b1_reference_id` = null where `b1_reference_id` is not null");
    }

    public function getMappingsDefault(){
        return [
            'order_date' => 'SELECT o1.date_modified FROM ' . DB_PREFIX . 'order o1 LEFT JOIN ' . DB_PREFIX . 'order_history o2 on o2.order_id = o1.order_id where o1.order_id = %d',
            'order_no' =>  'SELECT o1.order_id FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'invoice_series' => 'SELECT o1.invoice_prefix FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'invoice_number' => 'SELECT LPAD(o1.invoice_no,6,0) FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'currency' => 'SELECT o1.currency_code FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'shipping_amount' => 'SELECT o2.value FROM ' . DB_PREFIX . 'order o1 LEFT JOIN  ' . DB_PREFIX . 'order_total o2 on o2.order_id = o1.order_id where o2.code = ' . '\'shipping\'' . ' AND o1.order_id = %d',
            'discount' => 'SELECT o2.value FROM ' . DB_PREFIX . 'order o1 LEFT JOIN  ' . DB_PREFIX . 'order_total o2 on o2.order_id = o1.order_id where o2.code = ' . '\'coupon\'' . ' AND o1.order_id = %d',
            'gift'=>'SELECT o2.value FROM ' . DB_PREFIX . 'order o1 LEFT JOIN  ' . DB_PREFIX . 'order_total o2 on o2.order_id = o1.order_id where o2.code = ' . '\'voucher\'' . ' AND o1.order_id = %d',
            'total' => 'SELECT o2.value FROM ' . DB_PREFIX . 'order o1 LEFT JOIN  ' . DB_PREFIX . 'order_total o2 on o2.order_id = o1.order_id where o2.code = ' . '\'total\'' . ' AND o1.order_id = %d',
            'order_email' => 'SELECT o1.email FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'billing_is_company' => 'SELECT o1.payment_company FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'billing_first_name' => 'SELECT o1.payment_firstname FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'billing_last_name' => 'SELECT o1.payment_lastname FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'billing_address' => 'SELECT o1.payment_address_1 FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'billing_city' => 'SELECT o1.payment_city FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'billing_country' => 'SELECT o2.iso_code_2 FROM ' . DB_PREFIX . 'order o1 LEFT JOIN  ' . DB_PREFIX . 'country o2 on o1.payment_country_id = o2.country_id where o1.order_id = %d',
            'billing_short_name' => '',
            'billing_vat_code' => '',
            'billing_postcode' => '',
            'billing_code' => '',
            'delivery_is_company' => 'SELECT o1.shipping_company FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'delivery_first_name' => 'SELECT o1.shipping_firstname FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'delivery_last_name' => 'SELECT o1.shipping_lastname FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'delivery_address' => 'SELECT o1.shipping_address_1 FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'delivery_city' => 'SELECT o1.shipping_city FROM ' . DB_PREFIX . 'order o1 where o1.order_id = %d',
            'delivery_country' => 'SELECT o2.iso_code_2 FROM ' . DB_PREFIX . 'order o1 LEFT JOIN  ' . DB_PREFIX . 'country o2 on o1.shipping_country_id = o2.country_id where o1.order_id = %d',
            'delivery_short_name' => '',
            'delivery_vat_code' => '',
            'delivery_postcode' => '',
            'delivery_code' => '',
            'payer_name' => '',
            'payer_code' => '',
            'payer_vat_code' => '',
            'payer_address' => '',
            'payer_country_code' => '',
            'payment_code' => '',
            'payment_id' => '',
            'payment_payment_date' => '',
            'payment_sum' => '',
            'payment_tax' => '',
            'payment_currency' => '',
            'payment_payment' => '',
            'items_name' => 'SELECT o1.name FROM ' . DB_PREFIX . 'order_product o1 where o1.order_product_id = %d',
            'items_quantity' => 'SELECT o1.quantity FROM ' . DB_PREFIX . 'order_product o1 where o1.order_product_id = %d',
            'items_price' => 'SELECT o1.price FROM ' . DB_PREFIX . 'order_product o1 where o1.order_product_id = %d',
            'items_sum' => 'SELECT o1.total FROM ' . DB_PREFIX . 'order_product o1 where o1.order_product_id = %d',
            'items_vat_rate' => '',
            'items_code' => '',
        ];
    }

    public function resetMappings()
    {
        $mappingDefaults = $this->getMappingsDefault();
        $check = [
            'order_date',
            'order_no',
            'invoice_number',
            'invoice_series',
            'currency',
            'discount',
            'discountVat',
            'gift',
            'total',
            'order_email',
            'billing_is_company',
            'billing_first_name',
            'billing_last_name',
            'billing_address',
            'billing_city',
            'billing_country',
            'billing_short_name',
            'billing_vat_code',
            'billing_code',
            'billing_postcode',
            'delivery_is_company',
            'delivery_first_name',
            'delivery_last_name',
            'delivery_address',
            'delivery_city',
            'delivery_country',
            'delivery_short_name',
            'delivery_vat_code',
            'delivery_code',
            'delivery_postcode',
            'payer_name',
            'payer_code',
            'payer_vat_code',
            'payer_address',
            'payer_country_code',
            'payment_code',
            'payment_id',
            'payment_payment_date',
            'payment_sum',
            'payment_tax',
            'payment_currency',
            'payment_payment',
            'shipping_amount',
            'shipping_amount_tax',
            'items_name',
            'items_vat_rate',
            'items_quantity',
            'items_price',
            'items_price_vat',
            'items_code',
        ];
        $this->update($check, $mappingDefaults);
    }

    public function getSettingsDefault(){
        return [
            'sync_invoice' => 0,
            'quantity_sync' => 0,
            'sync_item_name' => 0,
            'sync_item_price' => 0,
            'write_off' => 0,
            'orders_sync_from' => date("Y-m-d"),
            'orders_sync_to' => null,
            'order_status_id' => 5,
        ];
    }
    public function resetSettings()
    {
        $settingDefaults = $this->getSettingsDefault();
        $check = [
            'sync_invoice',
            'quantity_sync',
            'sync_item_name',
            'sync_item_price',
            'write_off',
            'orders_sync_from',
            'orders_sync_to',
            'order_status_id',
        ];
        $this->update($check, $settingDefaults);
    }
}
