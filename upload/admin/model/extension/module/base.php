<?php

class ModelExtensionModuleBase extends Model
{

    public static function productTableName()
    {
        return DB_PREFIX . 'product';
    }

    public static function productDescriptionTableName()
    {
        return DB_PREFIX . 'product_description';
    }

    public static function orderTableName()
    {
        return DB_PREFIX . 'order';
    }

    public static function itemsTableName()
    {
        return DB_PREFIX . 'b1_items';
    }

    public function cronUrl($route, $params = array())
    {
        $this->load->model('extension/module/settings');
        $this->load->model('extension/module/items');
        $params['key'] = $this->model_extension_module_settings->get('cron_key');
        $url = $this->prepareUrl(str_replace('index.php', 'model/extension/module/b1_cron.php', $this->url->link($route, http_build_query($params), 'SSL')));
        $url = str_replace('route', 'id', $url);
        return $url;
    }

    public function cronProductImportUrl($route, $params = array())
    {
        $this->load->model('extension/module/settings');
        $this->load->model('extension/module/items');
        $params['key'] = $this->model_extension_module_settings->get('cron_key');
        $url = $this->prepareUrl(str_replace('index.php', 'product_import.php', $this->url->link($route, http_build_query($params), 'SSL')));
        $url = str_replace('route', 'id', $url);
        return $url;
    }

    protected function adminUrl($route, $params = array())
    {
        $params['user_token'] = $this->session->data['user_token'];
        return $this->prepareUrl($this->url->link($route, http_build_query($params), 'SSL'));
    }

    protected function prepareUrl($url)
    {
        return str_replace('&amp;', '&', $url);
    }
}
