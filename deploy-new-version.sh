#!/bin/bash

##########
# Deploy new version
##########

set -e # Exit on error
#set -x # See what is happening when executing the script.

# Make sure user specifies the new version
if [ $# -ne 1 ]; then
	echo "Please specify the new version."
	echo "Example: ./deploy-new-version.sh 2.0.0"
    exit 1
fi

git pull
sed -i "s/const VERSION = '.*';/const VERSION = '$1';/" upload/system/library/vendor/b1/libb1/B1.php
sed -i "s/const PLATFORM = '.*';/const PLATFORM = 'opencart';/" upload/system/library/vendor/b1/libb1/B1.php
zip -r B1Accounting.ocmod.zip upload install.xml
git commit -am "Bump plugin version"
git push
git tag -a $1 -m "New version"
git push --tags
